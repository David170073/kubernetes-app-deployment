# Kubernetes App Deployment

1. Before deploying Wordpress application you should clone the repository with `git clone https://gitlab.com/David170073/kubernetes-app-deployment.git`
2. To deploy the application you run the `kubectl apply -f .` command in the directory of the kubernetes manifiests that you cloned from this repository.
3. The app is available on {NodeIP}:30001, where NodeIP may change depending on the number of nodes and the loadbalancing that your cluster has. 
